//1

let userNumberFirst = prompt("Введіть перше число:");
while (isNaN(userNumberFirst)) {
    userNumberFirst = prompt("Це не число. Введіть перше число:");
}

let userNumberSecond = prompt("Введіть друге число:");
while (isNaN(userNumberSecond)) {
    userNumberSecond = prompt("Це не число. Введіть друге число:");
}

function getDividing(numberFirst, numberSecond) {
    if (numberSecond == 0) {
        console.log("На 0 не ділиться");
        return;
    }
    return numberFirst / numberSecond;
}

let resultDividing = getDividing(userNumberFirst, userNumberSecond);
console.log(resultDividing);



//2

let userNumberFirst2 = prompt("Введіть перше число:");
while (isNaN(userNumberFirst2)) {
    userNumberFirst2 = prompt("Це не число. Введіть перше число:");
}
userNumberFirst2 = +userNumberFirst2;

let userNumberSecond2 = prompt("Введіть друге число:");
while (isNaN(userNumberSecond2)) {
    userNumberSecond2 = prompt("Це не число. Введіть друге число:");
}
userNumberSecond2 = +userNumberSecond2;

let mathOperator;

while (true) {
    mathOperator = prompt("Введіть математичну операцію: +, -, * або /");
    if (mathOperator === "+" || mathOperator === "-" || mathOperator === "*" || mathOperator === "/") {
        break;
    } else {
        alert("Такої операції не існує. Спробуйте ще раз!");
    }
}


function calculateNumbers(numberFirst2, numberSecond2) {
    let result;    
    switch (mathOperator) {
        case '+':
            result = numberFirst2 + numberSecond2;
            break;
        case '-':
            result = numberFirst2 - numberSecond2;
            break;
        case '*':
            result = numberFirst2 * numberSecond2;
            break;
        case '/':
            if (numberSecond2 == 0) {
                console.log("На 0 не ділиться");
                return;
            }
            else {
                result = numberFirst2 / numberSecond2;
            }
            break;
        default:
    }
    return result;
}

console.log(calculateNumbers(userNumberFirst2, userNumberSecond2));


//3

let userNumberFactorial = prompt("Введіть число для підрахунку факторіалу:");
while (isNaN(userNumberFactorial)) {
    userNumberFactorial = prompt("Це не число. Введіть число:");
}

const factorializeNumber = (number) => {
    let resultFactorial = 1;
    for(let i = 1; i <= number; i++){
        resultFactorial = resultFactorial * i;
    }
    return resultFactorial;
};
console.log(factorializeNumber(userNumberFactorial));

